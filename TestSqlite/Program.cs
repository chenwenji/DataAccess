﻿using Machine.DataAccess;
using Machine.DataAccess.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Machine.DataAccess.Transaction;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Data.Common;
using System.Data;
//using System.Data.OleDb;
//using System.Threading;

namespace TestSqlite
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world");

            //var database = new DbSet<OrderInfo>().Where(x => x.o_id != "").Select(x => new { x.o_id, x.o_date });
            //var database = new DbSet<OrderDetail>().Join(new DbSet<OrderInfo>(), x => x.o_id, y => y.o_id, (y, z) => new { detail = y, order = z });
            //var database = new DbSet<OrderDetail>().Join(new DbSet<OrderInfo>(), x => x.o_id, y => y.o_id, (y, z) => new { y.od_id, z.c_id });
            //var database = new DbSet<OrderDetail>().Where(x => x.od_id != 0);
            var order = new DbSet<Employees>();
            //var detail = new DbSet<OrderDetail>();
            //var flag = order.AddOrUpdate(new Employees()
            //{
            //    e_account = "123",
            //    Id = "123"
            //});
            //Console.WriteLine(flag);
            var t = order.ToArray();
            foreach (var item in t)
            {
                Console.WriteLine(item.Id);
            }



            

            //var database = detail.Join(order, x => x.o_id, y => y.o_id, (x, y) => new { detail = x, order = y })
            //    .Where(x => x.detail.o_id == "O2008091632240078590").Distinct().Select(x => new { x.detail});
            //var database = detail.Where(x => x.order.o_id != "").Take(10).Select(x => new { x.od_id, x.o_id });
            //var database = detail.Where(x=>x.o_id == "O2008091632240078590").FirstOrDefault();
            //Console.WriteLine(database.o_id);
            //foreach (var item in database)
            //{
            //    Console.WriteLine(item);
            //}
        }
    }
    public enum LabelType
    {
        All,
        None
    }
    public class Employees
    {
        public string Id { get; set; }
        //public string e_account { get; set; }
        public string Name { get; set; }
        public LabelType LabelType { get; set; }
        //public OrderInfo Order { get; set; }
    }

    public partial class OrderInfo
    {
        public string id { get; set; }
        public string o_type { get; set; }
        public string o_belongType { get; set; }
        public string o_way { get; set; }
        public long c_id { get; set; }
        public DateTime o_date { get; set; }
        public decimal o_estimatedAmount { get; set; }
        public decimal o_actualAmount { get; set; }
        public string o_labels { get; set; }
        public string o_remark { get; set; }
        public string o_state { get; set; }
        public string o_settle { get; set; }
    }

    public class OrderDetail
    {
        public long od_id { get; set; }
        public string o_id { get; set; }
        public string TestName { get; set; }
        public OrderInfo order { get; set; }
    }
}
