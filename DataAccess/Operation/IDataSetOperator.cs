﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/25 10:13:15 
* clrversion :4.0.30319.18444
******************************************************/

using Machine.DataAccess.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Operation
{
    internal interface IDataSetOperator<TEntity>
    {
        //ProviderElement ProviderElement { get; set; }
        TEntity GetByKey(object key);
        bool DeleteByKey(object key);
        bool AddOrUpdate(TEntity entity);
        bool Add(TEntity entity);
        bool Update(TEntity entity);
        bool Delete(TEntity entity);
    }
}
