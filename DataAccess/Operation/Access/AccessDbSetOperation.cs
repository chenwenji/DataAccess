﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2017/4/14 20:12:50 
* clrversion :4.0.30319.42000
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Operation.Access
{
    class AccessDbSetOperation<TEntity> : DataSetOperatorBase<TEntity>
    {
        public AccessDbSetOperation(ProviderElement providerElement) : base(providerElement) { }

        protected override string TableBeforeChar { get { return "["; } }

        protected override string TableAfterChar { get { return "]"; } }
    }
}
