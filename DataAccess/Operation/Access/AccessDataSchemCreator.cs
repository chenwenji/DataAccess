﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2017/4/14 20:12:28 
* clrversion :4.0.30319.42000
******************************************************/

using Machine.DataAccess.Linq;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Operation.Access
{
    class AccessDataSchemCreator : DataSchemCreatorBase
    {
        public AccessDataSchemCreator(ProviderElement providerElement) : base(providerElement) { }
        protected override string SQL_CreateTable
        {
            //create table Test1 (id counter primary key);
            get { return string.Format("CREATE TABLE [{0}]([{1}] {2} PRIMARY KEY)", TABLEARG_NAME, COLUMARG_NAME, COLUMTYPEARG_NAME); }
        }

        protected override string SQL_CreateBasic
        {
            //ALTER TABLE NewTable ADD COLUMN Field3 DATE
            get { return string.Format("ALTER TABLE [{0}] ADD COLUMN {1} {2}", TABLEARG_NAME, COLUMARG_NAME, COLUMTYPEARG_NAME); }
        }

        protected override string SQL_CreateRelation
        {
            get
            {
                return string.Format("ALTER TABLE [{0}] ADD CONSTRAINT {0}_{1}_relation FOREIGN KEY ({1}Relation) REFERENCES [{1}]({2}) ON DELETE CASCADE",
                  TABLEARG_NAME, OUTTERTABLE_NAME, OUTTERKEY_NAME);
            }
        }

        public override bool CreateTable(string tableName, string primaryKeyName, string primaryKeyType, bool isAutoKey = false)
        {
            var sql = this.SQL_CreateTable.Replace(TABLEARG_NAME, tableName)
                .Replace(COLUMARG_NAME, primaryKeyName);
            if (isAutoKey) sql = sql.Replace(COLUMTYPEARG_NAME, " COUNTER ");
            else sql = sql.Replace(COLUMTYPEARG_NAME, primaryKeyType);

            return DataResult.CodeFirstExcuteNonQuery(new Linq.TranslateResult(sql, new DbParameter[0]), this.ProviderElement);
        }

        public override bool CreateBaseType(Type table, System.Reflection.PropertyInfo colum)
        {
            var sql = this.SQL_CreateBasic.Replace(TABLEARG_NAME, table.GetTableName())
                .Replace(COLUMARG_NAME, colum.Name)
                .Replace(COLUMTYPEARG_NAME, this.GetDbType(colum.PropertyType));

            return DataResult.CodeFirstExcuteNonQuery(new Linq.TranslateResult(sql, new DbParameter[0]), this.ProviderElement);
        }

        public override bool CreateFK(string tableName, string columName, string columType, string outterTable, string outterKey)
        {
            var addColumn = string.Format("ALTER TABLE [{0}] ADD COLUMN [{1}Relation] {2}", tableName, outterTable,columType);
            var strTemp = SQL_CreateRelation.Replace(TABLEARG_NAME, tableName)
                .Replace(COLUMARG_NAME, columName)
                .Replace(COLUMTYPEARG_NAME, columType)
                .Replace(OUTTERTABLE_NAME, outterTable)
                .Replace(OUTTERKEY_NAME, outterKey);
            DataResult.CodeFirstExcuteNonQuery(new TranslateResult(addColumn, new DbParameter[0]), this.ProviderElement);
            return DataResult.CodeFirstExcuteNonQuery(new TranslateResult(strTemp, new DbParameter[0]), this.ProviderElement);
        }

        protected override string GetDbType(Type type)
        {
            if (type == typeof(Guid)) return "GUID";
            var code = Type.GetTypeCode(type);
            switch (code)
            {
                case TypeCode.Boolean:
                    return "BIT";
                case TypeCode.SByte:
                case TypeCode.Byte:
                    return "BINARY";
                case TypeCode.Char:
                    return "TEXT";
                case TypeCode.DateTime:
                    return "DATETIME";
                case TypeCode.Decimal:
                    return "CURRENCY";
                case TypeCode.Double:
                    return "DOUBLE";
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                    return "INTEGER";
                case TypeCode.Int64:
                    return "LONG";
                case TypeCode.Single:
                    return "SINGLE";
                case TypeCode.String:
                    return "MEMO";
                case TypeCode.Empty:
                case TypeCode.DBNull:
                case TypeCode.Object:
                default:
                    throw new NotSupportedException("Code first 异常类型");
            }
        }
    }
}
