﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/25 12:15:05 
* clrversion :4.0.30319.18444
******************************************************/

using Machine.DataAccess.Common.ORM;
using Machine.DataAccess.Linq;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Operation.Sqlite
{
    class SqliteDbSetOperation<TEntity> : DataSetOperatorBase<TEntity>//IDataSetOperator<TEntity>
    {
        public SqliteDbSetOperation(ProviderElement providerElement) : base(providerElement) { }
        //private static Lazy<SqliteDbSetOperation<TEntity>> _instance = new Lazy<SqliteDbSetOperation<TEntity>>(() =>
        //    {
        //        return new SqliteDbSetOperation<TEntity>();
        //    }, true);
        //public static SqliteDbSetOperation<TEntity> Instance { get { return _instance.Value; } }
        //private SqliteDbSetOperation() { }
        //public override bool Add(TEntity entity)
        //{
        //    var temp = Config.Instance.GetDbSchemCreator();
        //    temp.CreateTable(typeof(TEntity));
        //    return base.Add(entity);
        //}

        protected override string TableBeforeChar { get { return "`"; } }
        protected override string TableAfterChar { get { return "`"; } }

        public override bool Update(TEntity entity)
        {
            var creator = Config.Instance.GetDbSchemCreator(this.ProviderElement);
            if (creator != null && creator.ProviderElement.IsCodeFirst == true) creator.CreateTable(typeof(TEntity));

            var schem = Config.Instance.GetDbSchemInfo(this.ProviderElement);//GetKeyName(typeof(TEntity));
            //var properties = this.GetProperties(entity.GetType(), entity, schem);

            //foreach (var item in properties)
            //{
            //    if (item.Entity != null && item.Entity.GetType() != this.EntityType)
            //    {
            //        UpdateEntity(item.Entity, this.GetProperties(item.Entity.GetType(), item.Entity, schem), schem);
            //    }
            //}

            //var result = UpdateEntity(entity, properties, schem);

            //foreach (var item in properties)
            //{
            //    if (item.Entity == null)
            //    {
            //        var type = item.PropertyInvoker.PropertyInfo.ReflectedType;
            //        var relation = schem.GetTableRelationType(this.EntityType, type);
            //        if (relation == TableRelationType.One2Many || relation == TableRelationType.One2Zero) continue;
            //        var sql = string.Format("select {0} from {1}", item.CoumName, this.EntityType.GetTableName());
            //        var id = DataResult.GetResult<Guid>(new TranslateResult(sql, new DbParameter[0]), this.ProviderElement);
            //        if (id != default(Guid))
            //        {
            //            sql = string.Format("PRAGMA foreign_keys=1; delete from {0} where {1} = @{1}", type.GetTableName(), item.PropertyInvoker.PropertyName);
            //            var parameterName = string.Format("@{0}", item.PropertyInvoker.PropertyName);
            //            DataResult.ExecuteNonQuery(new TranslateResult(sql, new DbParameter[] { schem.CreateDbParameter(parameterName, id) }),
            //                this.ProviderElement);
            //        }
            //    }
            //    else
            //    {
            //        this.AddOrUpdateEntity(item.Entity, this.GetProperties(item.Entity.GetType(), item.Entity, schem), schem);
            //    }
            //}

            //return result;
            throw new NotSupportedException();
        }

        public override bool DeleteByKey(object key)
        {
            var creator = Config.Instance.GetDbSchemCreator(this.ProviderElement);
            if (creator != null && creator.ProviderElement.IsCodeFirst == true) creator.CreateTable(typeof(TEntity));

            var schem = Config.Instance.GetDbSchemInfo(this.ProviderElement);
            var cacheItem = schem.GetSchem(this.EntityType);//GetKeyName(typeof(TEntity));
            var entity = GetByKey(key);
            if (entity == null) return false;
            string sql = string.Format("PRAGMA foreign_keys=1; delete from {0} where {1} = @{1}", cacheItem.TableName, cacheItem.PrimaryKeyName);

            var parameters = new List<DbParameter>();// { new SqlParameter("id", key) };
            var parameter = schem.CreateDbParameter(string.Format("@{0}", cacheItem.PrimaryKeyName), key);
            parameters.Add(parameter);

            return DataResult.ExecuteNonQuery(new TranslateResult(sql, parameters), this.ProviderElement);
        }
    }
}
