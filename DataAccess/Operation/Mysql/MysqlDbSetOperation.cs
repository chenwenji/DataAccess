﻿/******************************************************
* author :  HZK
* email  :  icecoke-zk@live.com 
* history:  created by hzk 2015/8/16 23:25:50 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Operation.Mysql
{
    class MysqlDbSetOperation<TEntity> : DataSetOperatorBase<TEntity>
    {
        public MysqlDbSetOperation(ProviderElement providerElement) : base(providerElement)
        {
        }

        protected override string TableBeforeChar { get { return "`"; } }
        protected override string TableAfterChar { get { return "`"; } }
    }
}
