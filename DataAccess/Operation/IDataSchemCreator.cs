﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/31 16:13:19 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Machine.DataAccess.Operation
{
    interface IDataSchemCreator
    {
        bool CreateTable(Type type);
        //bool CreateTable(Type type, params PropertyInfo[] primaryKeys);
        //bool CreateBaseType(Type table, PropertyInfo colum);
        //bool CreateRelation(string firstTableName, string firstPropertyName, string firstPropertyType,
        //    string secondTableName, string secondPropertyName, string secondPropertyType);
    }

    enum DataBaseRelationType
    {
        One2One,One2Many,Many2Many
    };
}
