﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/8/20 0:15:52 
* clrversion :4.0.30319.42000
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Operation
{
    /// <summary>
    /// 外键关联信息
    /// </summary>
    class RelationInfo
    {
        public Type PKType { get; set; }
        public Type FKType { get; set; }
        //public RelationType RelationType { get; set; }
        public TableRelationType TableRelationType { get; set; }
    }

    enum TableRelationType
    {
        Zero2Zero,Zero2One,Zero2Many,One2Zero,One2One,One2Many,Many2Zero,Many2One,Many2Many
    }
    enum SingleTableRelationType
    {
        Zero, One, Many
    }
}
