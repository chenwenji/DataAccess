﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/17 13:16:43 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Linq
{
    public static class PagingExtensions
    {
        #region IQueryable<T> 的拓展方法
        /// <summary>
        /// 把数据转化成为页面类型
        /// </summary>
        /// <typeparam name="T">T,泛型</typeparam>
        /// <param name="source">源数据</param>
        /// <param name="pageIndex">需要获取的页面索引</param>
        /// <param name="pageSize">页面的容量</param>
        /// <param name="totalCount">所有数据数,可以为空值</param>
        /// <returns>页面类型数据的接口</returns>
        public static IPageList<T> ToPageList<T>(this IOrderedQueryable<T> source, int pageIndex, int pageSize, int? totalCount = null)
        {
            return new PageList<T>(source, pageIndex, pageSize, totalCount);
        }

        public static IPageList<T> ToPageList<T>(this IOrderedEnumerable<T> source, int pageIndex, int pageSize, int? totalCount = null)
        {
            return new PageList<T>(source, pageIndex, pageSize, totalCount);
        }


        //public static IPageList<T> ToPagedList<T>(this IEnumerable<T> source, int pageIndex, int pageSize, int? totalCount = null)
        //{
        //    return new PageList<T>(source, pageIndex, pageSize, totalCount);
        //}


        #endregion
    }
}
