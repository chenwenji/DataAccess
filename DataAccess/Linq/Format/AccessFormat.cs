﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2017/4/16 23:05:46 
* clrversion :4.0.30319.42000
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Linq
{
    class AccessFormat : SetFormatBase
    {
        public AccessFormat(ProviderElement provider) : base(provider) { }

        protected override System.Linq.Expressions.Expression VisitTable(Relation.TableExpression table)
        {
            this.build.Append("[");
            this.build.Append(table.TableName);
            this.build.Append("]");
            this.build.Append(" as ");
            this.build.Append(table.TableArg);
            return table;
        }
    }
}
