﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:25:04 
* clrversion :4.0.30319.18444
******************************************************/

using Machine.DataAccess.Linq.Relation;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Machine.DataAccess.Linq
{
    class SQLServerFormat : SetFormatBase
    {
        public SQLServerFormat(ProviderElement providerElement) : base(providerElement) { }
        protected override Expression VisitTable(TableExpression table)
        {
            this.build.Append("[");
            this.build.Append(table.TableName);
            this.build.Append("]");
            this.build.Append(" as ");
            this.build.Append(table.TableArg);
            return table;
        }
    }
}
