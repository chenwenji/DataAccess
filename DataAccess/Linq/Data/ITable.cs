﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/8/25 7:43:18 
* clrversion :4.0.30319.42000
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Linq
{
    /// <summary>
    /// 表格的接口
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface ITable<TEntity>:IEnumerable<TEntity>
    {
        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="entty"></param>
        /// <returns></returns>
        bool Add(TEntity entty);
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Update(TEntity entity);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Delete(TEntity entity);

        //bool AddOrUpdate(TEntity entity);
    }
}
