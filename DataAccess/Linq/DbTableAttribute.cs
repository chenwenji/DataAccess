﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/17 9:40:11 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Linq
{
    /// <summary>
    /// 映射数据库的表
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DbTableAttribute:Attribute
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; private set; }
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="tableName">映射的表名</param>
        public DbTableAttribute(string tableName)
        {
            this.TableName = tableName.ToLower();
        }
    }
}
