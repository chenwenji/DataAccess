﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/17 13:15:21 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace System.Linq
{
    public interface IPageList : INotifyPropertyChanged
    {
        /// <summary>
        /// 页面数
        /// </summary>
        int PageCount { get; }
        /// <summary>
        /// 总项目数
        /// </summary>
        int TotalItemCount { get; }
        /// <summary>
        /// 当前页面索引,从0开始
        /// </summary>
        int PageIndex { get; }
        /// <summary>
        /// 当前页面数，从1开始
        /// </summary>
        int PageNumber { get; }
        /// <summary>
        /// 单页页面容量
        /// </summary>
        int PageSize { get; }
        /// <summary>
        /// 是否有前一页
        /// </summary>
        bool HasPreviousPage { get; }
        /// <summary>
        /// 是否有下一页
        /// </summary>
        bool HasNextPage { get; }
        /// <summary>
        /// 是否第一页
        /// </summary>
        bool IsFirstPage { get; }
        /// <summary>
        /// 是否最后一页
        /// </summary>
        bool IsLastPage { get; }
        /// <summary>
        /// 当前页面起点数
        /// </summary>
        int ItemStart { get; }
        /// <summary>
        /// 当前页面终点数
        /// </summary>
        int ItemEnd { get; }
    }

    public interface IPageList<TEntity> : IPageList, IEnumerable<TEntity>,INotifyPropertyChanged
    {
    }
}
