﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/8/6 16:37:10 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Machine.DataAccess.Linq.Relation
{
    class LikeExpression : Expression
    {
        public LikeExpression(Type type, Expression source)
            : base((ExpressionType)ProjectionType.Like, type)
        {
            this.Source = source;
        }
        public Expression Source { get; private set; }
    }
}
