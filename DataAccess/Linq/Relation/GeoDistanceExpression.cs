﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/12/1 16:40:08 
* clrversion :4.0.30319.42000
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Machine.DataAccess.Linq.Relation
{
    class GeoDistanceExpression : Expression
    {
        public GeoDistanceExpression(Type type)
            : base((ExpressionType)ProjectionType.GeoDistance, type)
        {
        }

        //public string PrimaryKeyName { get; set; }
        public int OldTableIndex { get; set; }
        public int TableIndex { get; set; }
        public Type TableType { get; set; }
        //public string TableName { get; set; }
        public string LatName { get; set; }
        public string LngName { get; set; }
        public string DistanceName { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
