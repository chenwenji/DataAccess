﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:19:56 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Machine.DataAccess.Linq.Relation
{
    class ProjectionExpression : Expression
    {
        public ProjectionExpression(SelectExpression select, Expression project)
            : base((ExpressionType)ProjectionType.Project, project.Type)
        {
            this.Selection = select;
            this.Projection = project;
        }
        public SelectExpression Selection { get; private set; }
        public Expression Projection { get; private set; }
    }
}
