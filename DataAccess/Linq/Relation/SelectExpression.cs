﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:20:39 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Machine.DataAccess.Linq.Relation
{
    class SelectExpression : Expression
    {
        public SelectExpression(Type type, int tableIndex, IEnumerable<ColumnDeclaration> colums, Expression from, Expression where, IEnumerable<ColumnDeclaration> orderBy) :
            base((ExpressionType)ProjectionType.Select, type)
        {
            this.Colums = colums;
            this.TableIndex = tableIndex;
            this.From = from;
            this.Where = where;
            this.OrderBy = orderBy;
            this.OrderByType = OrderByType.None;
            this.JoinSingleType = new List<TableExpression>();
        }

        public IEnumerable<ColumnDeclaration> Colums { get; internal set; }
        public string TableArg { get { return string.Format("t{0}", TableIndex); } }
        public Expression From { get; internal set; }
        public Expression Where { get; internal set; }
        public Expression Like { get; internal set; }
        public Expression GeoDistance { get; internal set; }
        public IEnumerable<ColumnDeclaration> OrderBy { get; internal set; }
        public int TableIndex { get; private set; }
        public int Top { get; internal set; }
        public int Skip { get; internal set; }
        public bool IsDistinct { get; set; }
        public OrderByType OrderByType { get; internal set; }
        public IEnumerable<TableExpression> JoinSingleType { get; set; }
        public bool IsCount { get; set; }
        public bool IsDelete { get; set; }
        public bool IsUpdate { get; set; }
        public Expression UpdateSelect { get; set; }
    }
}
