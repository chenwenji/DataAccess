﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:22:08 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Machine.DataAccess.Linq.Relation
{
    class JoinExpression : Expression
    {
        public JoinExpression(Type type, Expression left, Expression right, Expression on)
            : base((ExpressionType)ProjectionType.Join, type)
        {
            this.Left = left;
            this.Right = right;
            this.On = on;
        }
        public Expression Left { get; private set; }
        public Expression Right { get; private set; }
        public Expression On { get; private set; }
    }
}
