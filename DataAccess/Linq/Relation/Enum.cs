﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:18:50 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Linq.Relation
{
    enum ProjectionType
    {
        Project = 1000, Select, Table, Colum, Join, Like,GeoDistance,Update//,OrderBy
    }

    enum ColumType
    {
        None, Max, Min, Sum
    }

    enum OrderByType
    {
        None, ASC, DESC
    }
}
