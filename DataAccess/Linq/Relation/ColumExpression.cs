﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:21:48 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Machine.DataAccess.Linq.Relation
{
    class ColumExpression : Expression
    {
        public ColumExpression(Type type, int tableIndex, string name,string declareTypeName,string parentTypeName)
            : base((ExpressionType)ProjectionType.Colum, type)
        {
            //this.TableArg = tableArg;
            this.Name = name;
            this.TableIndex = tableIndex;
            this.ColumType = ColumType.None;
            this.DeclareTypeName = declareTypeName.ToLower();
            this.ParentTypeName = parentTypeName.ToLower();
        }
        //public ColumExpression(Type type, int tableIndex, string name)
        //    : this(type, string.Format("t{0}", tableIndex), name) { }
        public string TableArg { get { return string.Format("t{0}", this.TableIndex); } }
        public string Name { get; private set; }
        public int TableIndex { get; set; }
        public ColumType ColumType { get; internal set; }
        public string DeclareTypeName { get; private set; }
        public string ParentTypeName { get; internal set; }

        //public ColumExpression GetWrapColumExpression()
        //{
        //    return new ColumExpression(this.Type, this.TableIndex + 1, this.Name);
        //}
    }
}
