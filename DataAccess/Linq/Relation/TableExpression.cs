﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:21:03 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Machine.DataAccess.Linq.Relation
{
    class TableExpression : Expression
    {
        public TableExpression(Type type, string tableName, int tableIndex)
            : base((ExpressionType)ProjectionType.Table, type)
        {
            //this.TableArg = tableArg;
            this.TableIndex = tableIndex;
            this.TableName = tableName;
        }
        public Type DeclareType { get; internal set; }
        public string TableName { get; private set; }
        public string TableArg { get { return string.Format("t{0}", TableIndex); } }
        public int TableIndex { get; private set; }
    }
}
