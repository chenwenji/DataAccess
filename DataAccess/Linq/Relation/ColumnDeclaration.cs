﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:21:27 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Linq.Relation
{
    class ColumnDeclaration
    {
        public ColumnDeclaration(string name, ColumExpression columExpression)
        {
            this.ColumExpression = columExpression;
            this.ColumName = name;
        }

        public ColumExpression ColumExpression { get; private set; }
        public string ColumName { get; private set; }
    }
}
