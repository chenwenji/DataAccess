﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Machine.DataAccess.Linq.Relation
{
    class UpdateExpression: Expression
    {
        public UpdateExpression(Type type, Expression source) 
            : base((ExpressionType) ProjectionType.Update, type)
        {
            this.Set = source;
        }

        public Expression Set { get; private set; }
    }
}
