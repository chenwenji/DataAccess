﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:13:07 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Machine.DataAccess.Common.ORM
{
    class FieldInvoker<TTarget, TValue> : IPropertyInvoker
    {
        private FieldInfo fieldInfo;

        public FieldInvoker(FieldInfo info)
        {
            this.fieldInfo = info;
        }

        public PropertyInfo PropertyInfo { get { return null; } }

        public string PropertyName { get { return fieldInfo.Name; } }

        public void SetValue(object target, object value)
        {
            this.fieldInfo.SetValue(target, value);
        }

        public object GetValue(object target)
        {
            return this.fieldInfo.GetValue(target);
        }
    }
}
