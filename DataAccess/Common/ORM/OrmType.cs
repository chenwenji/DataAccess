﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:16:37 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess.Common.ORM
{
    internal enum OrmType
    {
        None, Basic, KnowType, UnKnowType
    }
}
