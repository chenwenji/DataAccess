﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:13:47 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Machine.DataAccess.Common.ORM
{
    class PropertyInvokerDic : Dictionary<string, IPropertyInvoker>
    {
        public Type Type { get; set; }
        public PropertyInvokerDic(Type type)
        {
            this.Type = type;
        }
        public void Add(PropertyInfo propertyInfo)
        {
            var name = propertyInfo.Name.ToLower();
            if (this.ContainsKey(name)) return;
            var createType = typeof(PropertyInvoker<,>).MakeGenericType(propertyInfo.ReflectedType, propertyInfo.PropertyType);
            var creatObj = Activator.CreateInstance(createType, propertyInfo) as IPropertyInvoker;
            this.Add(name, creatObj);
        }

        public void Add(FieldInfo field)
        {
            var name = field.Name.ToLower();
            if (this.ContainsKey(name)) return;
            var createType = typeof(FieldInvoker<,>).MakeGenericType(field.ReflectedType, field.FieldType);
            var creatObj = Activator.CreateInstance(createType, field) as IPropertyInvoker;
            this.Add(name, creatObj);
        }
    }
}
