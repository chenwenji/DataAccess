﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:12:03 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Machine.DataAccess.Common.ORM
{
    /// <summary>
    /// 委托访问对象接口
    /// </summary>
    public interface IPropertyInvoker
    {
        /// <summary>
        /// 属性名
        /// </summary>
        string PropertyName { get; }
        /// <summary>
        /// 设置属性值
        /// </summary>
        /// <param name="target">需要设置的类对象</param>
        /// <param name="value">值</param>
        void SetValue(object target, object value);
        /// <summary>
        /// 得到属性值
        /// </summary>
        /// <param name="target">需要得到的值得对象</param>
        /// <returns>值</returns>
        object GetValue(object target);
        /// <summary>
        /// 当前访问的属性
        /// </summary>
        PropertyInfo PropertyInfo { get; }
    }
}
