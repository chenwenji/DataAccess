﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/8/12 14:48:37 
* clrversion :4.0.30319.42000
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soul.DataAccess.Common
{
    /// <summary>
    /// 当前上下文信息容器
    /// </summary>
    /// <typeparam name="TScopData"></typeparam>
    [Serializable]
    public class ExecutionContext<TScopData> where TScopData : class, ICloneable
    {
        [ThreadStatic]
        private static ExecutionContext<TScopData> _current;

        public TScopData ScopData { get; private set; }

        internal ExecutionContext(TScopData scop)
        {
            if (scop == null)
            {
                throw new ArgumentNullException("上下文参数不能为空");
            }
            ScopData = scop;
        }

        public static ExecutionContext<TScopData> Current
        {
            get { return _current; }
            internal set { _current = value; }
        }
        public DependentContext<TScopData> DepedentClone()
        {
            return new DependentContext<TScopData>(this);
        }
    }
}
