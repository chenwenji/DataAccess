﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/12/15 18:54:21 
* clrversion :4.0.30319.42000
******************************************************/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess
{
    internal class ConcurrentSet<T> : ConcurrentBag<T>
    {
        public ConcurrentSet(IEnumerable<T> collection) : base(collection) { }
        public ConcurrentSet() { }

        public new void Add(T item)
        {
            if (!this.Contains(item)) base.Add(item);
        }
    }
}
