﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/8/12 14:48:06 
* clrversion :4.0.30319.42000
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Soul.DataAccess.Common
{
    /// <summary>
    /// 多线程上下文信息容器
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    [Serializable]
    public class DependentContext<TEntity> : ExecutionContext<TEntity> where TEntity : class, ICloneable
    {
        public Thread OriginalThread { get; private set; }
        public DependentContext(ExecutionContext<TEntity> context)
            : base(context.ScopData.Clone() as TEntity)
        {
            this.OriginalThread = Thread.CurrentThread;
        }
    }
}
