﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/12/1 11:47:35 
* clrversion :4.0.30319.42000
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreColumnAttribute : Attribute { }
}
