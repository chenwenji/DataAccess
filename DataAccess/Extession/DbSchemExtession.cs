﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/28 16:46:44 
* clrversion :4.0.30319.18444
******************************************************/

using Machine.DataAccess.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machine.DataAccess
{
    static class DbSchemExtession
    {
        static ConcurrentDictionary<Type, DbTableAttribute> _tableNameCache = new ConcurrentDictionary<Type, DbTableAttribute>();
        public static string GetTableName(this Type type)
        {
            if (_tableNameCache.ContainsKey(type)) return _tableNameCache[type].TableName;
            var tableAttribute = type.GetCustomAttributes(typeof(DbTableAttribute), true).FirstOrDefault() as DbTableAttribute;
            tableAttribute = tableAttribute == null ? new DbTableAttribute(type.Name.ToLower()) : tableAttribute;
            _tableNameCache[type] = tableAttribute;
            return tableAttribute.TableName;
        }
    }
}
