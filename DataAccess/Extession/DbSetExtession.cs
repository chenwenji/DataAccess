﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/20 15:37:02 
* clrversion :4.0.30319.18444
******************************************************/

namespace Machine.DataAccess.Linq
{
    /// <summary>
    /// 数据访问的拓展方法
    /// </summary>
    public static class DbSetExtession
    {
        /// <summary>
        /// 根据主键得到信息
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="context"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static TEntity GetByKey<TEntity>(this DbSet<TEntity> context, object key)
        {
            var dataSetOperator = Config.Instance.GetDataSetOperator<TEntity>(context.ProviderElement);
            return dataSetOperator.GetByKey(key);
        }

        /// <summary>
        /// 根据主键删除对象
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="context"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool DeleteByKey<TEntity>(this DbSet<TEntity> context, object key)
        {
            var dataSetOperator = Config.Instance.GetDataSetOperator<TEntity>(context.ProviderElement);
            return dataSetOperator.DeleteByKey(key);
        }

        public static bool Delete<TEntity>(this DbSet<TEntity> context, TEntity key)
        {
            var dataSetOperator = Config.Instance.GetDataSetOperator<TEntity>(context.ProviderElement);
            return dataSetOperator.Delete(key);
        }

        /// <summary>
        /// 增加或更新对象
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="context"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static bool AddOrUpdate<TEntity>(this DbSet<TEntity> context, TEntity entity)
        {
            var dataSetOperator = Config.Instance.GetDataSetOperator<TEntity>(context.ProviderElement);
            return dataSetOperator.AddOrUpdate(entity);
        }

        /// <summary>
        /// 增加一个对象
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="context"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static bool Add<TEntity>(this DbSet<TEntity> context, TEntity entity)
        {
            var dataSetOperator = Config.Instance.GetDataSetOperator<TEntity>(context.ProviderElement);
            return dataSetOperator.Add(entity);
        }

        /// <summary>
        /// 更新一个对象
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="context"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static bool Update<TEntity>(this DbSet<TEntity> context, TEntity entity)
        {
            var dataSetOperator = Config.Instance.GetDataSetOperator<TEntity>(context.ProviderElement);
            return dataSetOperator.Update(entity);
        }
    }
}
