﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:31:48 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soul.DataAccess.Procedure
{
    /// <summary>
    /// 标示方法是否存储过程
    /// </summary>
    [Obsolete("不推荐使用")]
    [AttributeUsage(AttributeTargets.Method)]
    public class ProcedureAttribute : Attribute
    {
        /// <summary>
        /// 存储过程名
        /// </summary>
        public string ProcedureName { get; private set; }
        /// <summary>
        /// 存储过程返回的结果类型
        /// </summary>
        public ProcedureReturnType ProcedureReturnType { get; private set; }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="procedureName">存储过程名</param>
        /// <param name="procedureReturnType">存储过程返回结果,默认为返回单个结果集</param>
        public ProcedureAttribute(string procedureName, ProcedureReturnType procedureReturnType = ProcedureReturnType.DbSet)
        {
            this.ProcedureName = procedureName;
            this.ProcedureReturnType = procedureReturnType;
        }
    }
}
