﻿/******************************************************
* author :  cwj
* email  :  chenwenji_360@live.com 
* history:  created by cwj 2015/7/16 16:31:30 
* clrversion :4.0.30319.18444
******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Soul.DataAccess.Procedure
{
    /// <summary>
    /// 存储过程返回的值类型
    /// </summary>
    [Obsolete("不推荐使用")]
    public enum ProcedureReturnType
    {
        /// <summary>
        /// 单个数据及
        /// </summary>
        DbSet, 
        /// <summary>
        /// 单个结果，非数据集
        /// </summary>
        Single, 
        /// <summary>
        /// 没有返回值
        /// </summary>
        NoData
    };
}
